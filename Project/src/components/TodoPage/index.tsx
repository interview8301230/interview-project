import { Link } from "react-router-dom";


function Todo() {
// Get The Selected TODO
  return (
    <div>
      <h2>Edit a task</h2>
      <div>
        <Link to="/">Back To Home</Link>
      </div>
        <form>
          <div>
            <input
              type="text"
              name="title"
              readOnly
            ></input>
          </div>
          <div>
            <input
              type="text"
              name="description"
              readOnly
            ></input>
          </div>
          <div>
            <select name="status">
              <option>choose a status</option>
            </select>
          </div>
          <button type="submit">Submit</button>
        </form>
       <div>
            <button
              onClick={() => {
                // Go to next TODO
              }}
            >
              Next
            </button>
            <button
              onClick={() => {
                // Go to Prev TODO
              }}
            >
              Prev
            </button>
          </div>
    </div>
  );
}

export default Todo;
