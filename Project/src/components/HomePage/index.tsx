import React from "react";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
function Home() {
  return (
    <div>
      <TodoForm />
      <TodoList />
    </div>
  );
}

export default Home;
